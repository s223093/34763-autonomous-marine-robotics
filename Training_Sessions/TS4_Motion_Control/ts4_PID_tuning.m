Kp = 50;
Ki = 5;
Kd = 2;

simout = sim("PID_tuning");

figure;
hold on;
plot(simout.ref,"r");
plot(simout.y,"b");
legend("ref","y");
grid on;
hold off;

% Extract the relevant data
time = simout.y.Time;
output = simout.y.Data;

% Analyze system response
info = stepinfo(output, time);

% Display the results
fprintf('Rise Time: %.4f seconds\n', info.RiseTime);
fprintf('Overshoot: %.4f%%\n', info.Overshoot);
fprintf('Settling Time: %.4f seconds\n', info.SettlingTime);